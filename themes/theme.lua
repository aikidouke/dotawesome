theme                               = {}

theme.wallpaper                     = "~/background.png"

theme.font                          = "Liberation Sans 9"
theme.taglist_font                  = "Liberation Mono 11"

theme.color_dark                    = "#111111"
theme.color_light                   = "#222222"
theme.highlight_dark                = "#336699"
theme.highlight_light               = "#88AADD"
theme.text_dark                     = "#999999"
theme.text_light                    = "#EEEEEE"

--theme.fg_normal                     = "#FFFFFF"
--theme.bg_focus                      = "#444444"
theme.fg_focus                      = theme.highlight_dark
theme.bg_normal                     = theme.color_light
theme.fg_urgent                     = "#CC9393"
theme.bg_urgent                     = "#2A1F1E"
theme.border_width                  = "2"
theme.border_normal                 = theme.color_dark
theme.border_focus                  = theme.color_dark
theme.taglist_bg_focus              = theme.color_light
theme.taglist_fg_normal             = theme.text_light
theme.taglist_fg_focus              = theme.text_light
theme.tasklist_bg_normal            = theme.color_light
theme.tasklist_bg_focus             = theme.highlight_dark
theme.tooltip_bg_color              = theme.color_light
theme.tooltip_fg_color              = theme.text_light
theme.bg_systray                    = theme.color_light

-- icon used when a client has no default
theme.generic_icon = "/usr/share/icons/breeze-dark/apps/48/utilities-terminal.svg"

-- layout images
theme.icon_dir                      = "/usr/share/awesome/themes/default/layouts"
theme.layout_tile                   = theme.icon_dir .. "/tilew.png"
theme.layout_tilegaps               = theme.icon_dir .. "/tilegapsw.png"
theme.layout_tileleft               = theme.icon_dir .. "/tileleftw.png"
theme.layout_tilebottom             = theme.icon_dir .. "/tilebottomw.png"
theme.layout_tiletop                = theme.icon_dir .. "/tiletopw.png"
theme.layout_fairv                  = theme.icon_dir .. "/fairvw.png"
theme.layout_fairh                  = theme.icon_dir .. "/fairhw.png"
theme.layout_spiral                 = theme.icon_dir .. "/spiralw.png"
theme.layout_dwindle                = theme.icon_dir .. "/dwindlew.png"
theme.layout_max                    = theme.icon_dir .. "/maxw.png"
theme.layout_fullscreen             = theme.icon_dir .. "/fullscreenw.png"
theme.layout_magnifier              = theme.icon_dir .. "/magnifierw.png"
theme.layout_floating               = theme.icon_dir .. "/floatingw.png"

-- titlebar images
theme.tb_button                    = os.getenv("HOME") .. "/.config/awesome/themes/tb_button.png"
theme.tb_color                     = os.getenv("HOME") .. "/.config/awesome/themes/tb_color.png"
theme.titlebar_close_button_normal = theme.tb_color
theme.titlebar_close_button_focus  = theme.tb_button
theme.titlebar_maximized_button_normal_inactive = theme.tb_color
theme.titlebar_maximized_button_focus_inactive = theme.tb_button
theme.titlebar_maximized_button_normal_active = theme.tb_color
theme.titlebar_maximized_button_focus_active = theme.tb_button
theme.titlebar_minimize_button_normal_inactive = theme.tb_color
theme.titlebar_minimize_button_focus_inactive = theme.tb_button
theme.titlebar_minimize_button_normal_active = theme.tb_color
theme.titlebar_minimize_button_focus_active = theme.tb_button

theme.taglist_squares_sel           = "light"
theme.taglist_squares_unsel         = "light"

-- lain related
theme.useless_gap_width             = 8
theme.lain_icons                    = os.getenv("HOME") .. "/.config/awesome/lain/icons/layout/default/"
theme.layout_termfair               = theme.lain_icons .. "termfairw.png"
theme.layout_cascade                = theme.lain_icons .. "cascadew.png"
theme.layout_cascadetile            = theme.lain_icons .. "cascadetilew.png"
theme.layout_centerwork             = theme.lain_icons .. "centerworkw.png"
theme.layout_uselesstile            = theme.layout_tile
theme.layout_uselesstileleft        = theme.layout_tileleft
theme.layout_uselesstiletop         = theme.layout_tiletop

return theme
