-- {{{ Required libraries
local gears     = require("gears")
local awful     = require("awful")
awful.rules     = require("awful.rules")
                  require("awful.autofocus")
local common =    require("awful.widget.common")
local wibox     = require("wibox")
local beautiful = require("beautiful")
local naughty   = require("naughty")
local lain      = require("lain")
local vicious   = require("vicious")
-- }}}

-- {{{ Notifications position, and border
naughty.config.defaults.position = "bottom_left"
naughty.config.defaults.border_width = 2
-- }}}

-- {{{ Error handling
if awesome.startup_errors then
    naughty.notify({ preset = naughty.config.presets.critical,
                     title = "Oops, there were errors during startup!",
                     text = awesome.startup_errors })
end

do
    local in_error = false
    awesome.connect_signal("debug::error", function (err)
        if in_error then return end
        in_error = true
        naughty.notify({ preset = naughty.config.presets.critical,
                         title = "Oops, an error happened!",
                         text = err })
        in_error = false
    end)
end
-- }}}

-- {{{ Markup Helper
markup = lain.util.markup
-- }}}

-- {{{{ More custom than usual
	-- {{{ Fix Mumble Notifications, custom colors
	naughty.config.notify_callback = function(args)
		if args.icon == "gtk-dialog-info" or args.icon == "gtk-edit" then
			args.text = string.gsub(args.text, "<[^>]+>", "")
		end
		naughty.config.defaults.fg = beautiful.text_light
		naughty.config.defaults.bg = beautiful.color_light
		naughty.config.defaults.border_color = beautiful.color_dark
		return args
	end
	-- }}}

	-- {{{ Custom version of awful.widget.common.list_update to create an icon-only vertical tasklist
	function verticaltask(w, buttons, label, data, objects)
		w:reset()
		for o = #objects, 1, -1 do
			local cache = data[objects[o]]
			local ib, bgb, m, l
			if cache then
				ib = cache.ib
				bgb = cache.bgb
				m   = cache.m
			else
				ib = wibox.widget.imagebox()
				m = wibox.layout.margin(ib, 3, 3, 6, 6)
				l = wibox.layout.fixed.vertical()
				l:fill_space(true)
				l:add(m)
				bgb = wibox.layout.margin(l, 0, 2, 0, 0)
				bgb:buttons(common.create_buttons(buttons, objects[o]))
				data[objects[o]] = {
					ib = ib,
					bgb = bgb,
					m   = m
				}
			end
			local text, bg, bg_image, icon = label(objects[o])
			bgb:set_color(bg)
			if icon then
				ib:set_image(icon)
			else
				ib:set_image(beautiful.generic_icon)
			end
			w:add(bgb)
	   end
	end
	-- }}}

	-- {{{ Another custom version of awful.widget.common.list_update for the taglist
	function verticaltag(w, buttons, label, data, objects)
		w:reset()
		for i, o in ipairs(objects) do
			local cache = data[o]
			local tb, bgb, m, ah, bgt, ms
			if cache then
				tb = cache.tb
				bgb = cache.bgb
				m = cache.m
				ah = cache.ah
				bgt = cache.bgt
				ms = cache.ms
			else
				tb = wibox.widget.textbox()
				bgb = wibox.widget.background()
				bgt = wibox.widget.background()
				ah = wibox.layout.align.horizontal()
				ah:set_middle(tb)
				bgt:set_widget(ah)
				ms = wibox.layout.margin(bgt, 0, 2, 0, 0)
				m = wibox.layout.margin(ms, 0, 0, 0, 2)
				bgb:set_bg(beautiful.border_normal)
				bgb:set_widget(m)
				bgb:buttons(common.create_buttons(buttons, o))
				data[o] = {
					tb = tb,
					bgb = bgb,
					m   = m,
					ah = ah,
					bgt = bgt,
					ms = ms
				}
			end
			local text, bg, bg_image, icon = label(o)
			if not pcall(tb.set_markup, tb, markup(beautiful.text_dark, text)) then
				tb:set_markup("<i>&lt;Invalid text&gt;</i>")
			end
			if bg_image == "light" then
				tb:set_markup(markup(beautiful.text_light, text))
			end
			bgt:set_bg(bg)
			if bg == beautiful.taglist_bg_focus then
				ms:set_color(beautiful.fg_focus)
			else
				ms:set_color(beautiful.border_normal)
			end
			w:add(bgb)
	   end
	end
	-- }}}
-- }}}}

-- {{{ Autostart applications
function run_once(cmd)
  findme = cmd
  firstspace = cmd:find(" ")
  if firstspace then
     findme = cmd:sub(0, firstspace-1)
  end
  awful.util.spawn_with_shell("pgrep -u $USER -x " .. findme .. " > /dev/null || (" .. cmd .. ")")
end

run_once("numlockx")
run_once("easystroke")
run_once("unclutter --exclude-root")
run_once("compton")
-- }}}

-- {{{ Variable definitions

-- beautiful init
beautiful.init("~/.config/awesome/themes/theme.lua")

-- common
modkey     = "Mod4"
altkey     = "Mod1"
terminal   = "termite"
editor     = "nvim"

local layouts = {
	lain.layout.uselesstile,
	lain.layout.centerwork,
	awful.layout.suit.max,
	awful.layout.suit.floating
}
-- }}}

-- {{{ Tags
tags = {}
for s = 1, screen.count() do
   tags[s] = awful.tag({"➊", "➋", "➌", "➍"}, s, layouts[1])
end
-- }}}

-- {{{ Wallpaper
if beautiful.wallpaper then
    for s = 1, screen.count() do
        gears.wallpaper.maximized(beautiful.wallpaper, s, true)
    end
end
-- }}}

-- {{{ Wibox

-- Net graph
nettext = wibox.widget.textbox()
nettext:set_align("center")
nettext:set_font("Liberation Sans 7")
netgraph = awful.widget.graph()
netgraph:set_height(24)
netgraph:set_stack(true)
netgraph:set_scale(true)
netgraph:set_stack_colors({ beautiful.highlight_light, beautiful.highlight_dark })
netgraph:set_background_color(beautiful.border_normal)
vicious.register(nettext, vicious.widgets.net,
	function(widget, args)
		local up = 0
		local down = 0
		local iface
		for name, value in pairs(args) do
			iface = name:match("^{(%S+) down_b}$")
			if iface and iface ~= "lo" then down = down + value end
			iface = name:match("^{(%S+) up_b}$")
			if iface and iface ~= "lo" then up = up + value end
		end
		netgraph:add_value(up, 1)
		netgraph:add_value(down, 2)
		local bps = function(val)
			if val > 1000000 then
				return string.format("%.0fM", val/1000000)
			elseif val > 1000 then
				return string.format("%.0fK", val/1000)
			end
			return string.format("%.0fB", val)
		end
		return (bps(up) .. "\n" .. bps(down))
	end, 2)
netback = wibox.widget.background(nettext, beautiful.border_normal)
netlayout = wibox.layout.fixed.vertical()
netlayout:add(netgraph)
netlayout:add(netback)
networkwidget = wibox.layout.margin(netlayout, 2, 0, 4, 4)
networkwidget:set_color(beautiful.color_dark)

-- CPU bars
cpulayout = wibox.layout.fixed.vertical()
cpubar = {}
cpumargin = {}
cores = 4 -- change to number of cpu cores
for i=1,cores do
	cpubar[i] = awful.widget.progressbar()
	cpubar[i]:set_width(24)
	cpubar[i]:set_height(2)
	cpubar[i]:set_background_color(beautiful.color_light)
	cpubar[i]:set_color({ type = "linear", from = { 20, 0 }, to = { 30,0 }, stops = { {0, beautiful.highlight_light}, {1, beautiful.highlight_dark }}})
	cpumargin[i] = wibox.layout.margin(cpubar[i], 0, 0, 1, 1)
	cpulayout:add(cpumargin[i])
end
cputext = wibox.widget.textbox()
cputest = vicious.register(cputext, vicious.widgets.cpu,
	function(widget, args)
		for i=1,cores do
			cpubar[i]:set_value(tonumber(args[i+1])/100)
		end
		return args[1]
	end, 2)
cpuwidget = wibox.layout.margin(cpulayout, 2, 0, 2, 2)
cpuwidget:set_color(beautiful.color_dark)

-- Memory bar
membar = awful.widget.progressbar()
membar:set_width(24)
membar:set_height(4)
membar:set_background_color(beautiful.color_light)
membar:set_border_color(nil)
membar:set_color({ type = "linear", from = { 20, 0 }, to = { 30,0 }, stops = { {0, beautiful.highlight_dark}, {1, beautiful.highlight_light }}})
vicious.register(membar, vicious.widgets.mem, "$1", 10)
memwidget = wibox.layout.margin(membar, 2, 0, 2, 2)
memwidget:set_color(beautiful.color_dark)

-- Battery
batbar = awful.widget.progressbar()
batbar:set_width(24)
batbar:set_height(6)
batbar:set_vertical(false)
batbar:set_background_color(beautiful.color_dark)
batwidget = wibox.layout.margin(batbar, 2, 2, 2, 2)
vicious.register(batbar, vicious.widgets.bat, 
	function(widget, args)
		if args[1] == "⌁" then
			batbar:set_height(0)
			batwidget:set_margins(0, 0, 0, 0)
		elseif args[1] == "+" then
			batbar:set_color(beautiful.highlight_dark)
		else
			batbar:set_color(beautiful.highlight_light)
			if args[2] < 15 then
				batbar:set_color("#FF0000")
			end
		end
		return args[2]
	end , 10, "BAT0")
batwidget:set_color(beautiful.color_light)

-- Volume
myvolumebar = lain.widgets.alsabar({
    card   = "0",
    width  = 24,
    heigth = 4, -- Typo in lain.widgets.alsabar
	step   = "5%", 
    colors = {
        background = beautiful.color_dark,
        unmute     = beautiful.highlight_dark,
        mute       = beautiful.text_dark
    },
    notifications = {
        font      = "Input",
        font_size = "12",
        color     = beautiful.text_light,
		bg_color  = beautiful.color_light
    }
})
volumewidget = wibox.layout.margin(myvolumebar.bar, 2, 2, 2, 4)
volumewidget:set_color(beautiful.color_light)

-- Time/Date/Calendar
timedate = wibox.widget.textbox()
timedate:set_align("center")
caltimer = timer({timeout = 60})
caltimer:connect_signal("timeout", 
	function()
		timedate:set_markup(string.gsub(os.date("%I:%M!%m/%d"), "0?([^0]?.:..)!0?([^0]?./..)", "%1\n%2", 1))
	end)
caltimer:start()
caltimer:emit_signal("timeout")
calendar = wibox.widget.background()
calendar:set_widget(timedate)
calendar:set_bg(beautiful.color_dark)
calendarwidget = wibox.layout.margin(calendar, 0, 0, 5, 5)
calendarwidget:set_color(beautiful.color_dark)
lain.widgets.calendar:attach(calendarwidget, { fg = beautiful.text_light, bg = beautiful.color_light, position = "bottom_left", font = "Input", followmouse = true })

-- Systray
systray = wibox.widget.systray()
systray:set_horizontal(false)
--systray:set_base_size(22)
systraymargin = wibox.layout.margin(systray, 0, 0, 0, 4)
systrayline = wibox.layout.margin(systraymargin, 0, 0, 0, 2)
systrayline:set_color(beautiful.color_dark)
systraywidget = wibox.layout.margin(systrayline, 4, 4, 4, 2)
systraywidget:set_color(beautiful.color_light)

-- Create a wibox for each screen and add it
borderbox = {}
mywibox = {}
mybottomwibox = {}
mylayoutbox = {}
mytaglist = {}
mytaglist.buttons = awful.util.table.join(
                    awful.button({ }, 1, awful.tag.viewonly),
                    awful.button({ modkey }, 1, awful.client.movetotag),
                    awful.button({ }, 3, awful.tag.viewtoggle),
                    awful.button({ modkey }, 3, awful.client.toggletag),
                    awful.button({ }, 4, function(t) awful.tag.viewprev(awful.tag.getscreen(t)) end),
                    awful.button({ }, 5, function(t) awful.tag.viewnext(awful.tag.getscreen(t)) end)
                    )
mytasklist = {}
mytasklist.buttons = awful.util.table.join(
                     awful.button({ }, 1, function (c)
                                              if c == client.focus then
                                                  c.minimized = true
                                              else
                                                  -- Without this, the following
                                                  -- :isvisible() makes no sense
                                                  c.minimized = false
                                                  if not c:isvisible() then
                                                      awful.tag.viewonly(c:tags()[1])
                                                  end
                                                  -- This will also un-minimize
                                                  -- the client, if needed
                                                  client.focus = c
                                                  c:raise()
                                              end
                                          end),
                     awful.button({ }, 3, function ()
                                              if instance then
                                                  instance:hide()
                                                  instance = nil
                                              else
                                                  instance = awful.menu.clients({ width=250 })
                                              end
                                          end),
                     awful.button({ }, 4, function ()
                                              awful.client.focus.byidx(1)
                                              if client.focus then client.focus:raise() end
                                          end),
                     awful.button({ }, 5, function ()
                                              awful.client.focus.byidx(-1)
                                              if client.focus then client.focus:raise() end
                                          end))

for s = 1, screen.count() do
    -- Create an imagebox widget which will contain an icon indicating which layout we're using.
    -- We need one layoutbox per screen.
    mylayoutbox[s] = wibox.layout.margin(awful.widget.layoutbox(s), 4, 4, 4, 4)
	mylayoutbox[s]:set_color(beautiful.color_light)
    mylayoutbox[s]:buttons(awful.util.table.join(
                           awful.button({ }, 1, function () awful.layout.inc(layouts, 1) end),
                           awful.button({ }, 3, function () awful.layout.inc(layouts, -1) end),
                           awful.button({ }, 4, function () awful.layout.inc(layouts, 1) end),
                           awful.button({ }, 5, function () awful.layout.inc(layouts, -1) end)))
    -- Create a taglist widget
    mytaglist[s] = awful.widget.taglist(s, awful.widget.taglist.filter.all, mytaglist.buttons, nil, verticaltag, wibox.layout.fixed.vertical())

    -- Create a tasklist widget
    mytasklist[s] = awful.widget.tasklist(s, awful.widget.tasklist.filter.currenttags, mytasklist.buttons, nil, verticaltask, wibox.layout.fixed.vertical())

	-- lain.widgets.borderbox doesn't seem to work well vertically, so lets make our own
    borderbox[s] = awful.wibox({ position = "left", screen = s, width = 33, bg = theme.border_focus })
    -- Create the wibox
    mywibox[s] = awful.wibox({ position = "left", screen = s, width = 31 })

    -- Widgets that are aligned to the top
    local top_layout = wibox.layout.fixed.vertical()
    top_layout:add(mytaglist[s])
    top_layout:add(mytasklist[s])

	-- Empty space in the middle.  Allows scrolling over the taskbar to switch windows
	local emptyspace = wibox.layout.flex.vertical()
	emptyspace:buttons(awful.util.table.join(
					 awful.button({ }, 4, function ()
											  awful.client.focus.byidx(1)
											  if client.focus then client.focus:raise() end
										  end),
					 awful.button({ }, 5, function ()
											  awful.client.focus.byidx(-1)
											  if client.focus then client.focus:raise() end
										  end)))

    -- Widgets that are aligned to the bottom
    local bottom_layout = wibox.layout.fixed.vertical()
    bottom_layout:add(systraywidget)
    bottom_layout:add(mylayoutbox[s])
	bottom_layout:add(volumewidget)
	bottom_layout:add(batwidget)
	bottom_layout:add(networkwidget)
	bottom_layout:add(cpuwidget)
	bottom_layout:add(memwidget)
    bottom_layout:add(calendarwidget)

    -- Now bring it all together (with the tasklist in the middle)
    local layout = wibox.layout.align.vertical()
    layout:set_top(top_layout)
	layout:set_middle(emptyspace)
    layout:set_bottom(bottom_layout)

    mywibox[s]:set_widget(layout)
end
-- }}}

-- {{{ Mouse bindings
root.buttons(awful.util.table.join(
    awful.button({ }, 4, awful.tag.viewprev),
    awful.button({ }, 5, awful.tag.viewnext)
))
-- }}}

-- {{{ Key bindings
globalkeys = awful.util.table.join(
    -- Tag browsing
    awful.key({ modkey }, "Left",   awful.tag.viewprev       ),
    awful.key({ modkey }, "Right",  awful.tag.viewnext       ),
    awful.key({ modkey }, "Escape", awful.tag.history.restore),

    -- Non-empty tag browsing
    awful.key({ altkey }, "Left", function () lain.util.tag_view_nonempty(-1) end),
    awful.key({ altkey }, "Right", function () lain.util.tag_view_nonempty(1) end),

    -- Default client focus
    awful.key({ altkey }, "k",
        function ()
            awful.client.focus.byidx( 1)
            if client.focus then client.focus:raise() end
        end),
    awful.key({ altkey }, "j",
        function ()
            awful.client.focus.byidx(-1)
            if client.focus then client.focus:raise() end
        end),

    -- By direction client focus
    awful.key({ modkey }, "j",
        function()
            awful.client.focus.bydirection("down")
            if client.focus then client.focus:raise() end
        end),
    awful.key({ modkey }, "k",
        function()
            awful.client.focus.bydirection("up")
            if client.focus then client.focus:raise() end
        end),
    awful.key({ modkey }, "h",
        function()
            awful.client.focus.bydirection("left")
            if client.focus then client.focus:raise() end
        end),
    awful.key({ modkey }, "l",
        function()
            awful.client.focus.bydirection("right")
            if client.focus then client.focus:raise() end
        end),

    -- Layout manipulation
    awful.key({ modkey, "Shift"   }, "j", function () awful.client.swap.byidx(  1)    end),
    awful.key({ modkey, "Shift"   }, "k", function () awful.client.swap.byidx( -1)    end),
    awful.key({ modkey, "Control" }, "j", function () awful.screen.focus_relative( 1) end),
    awful.key({ modkey, "Control" }, "k", function () awful.screen.focus_relative(-1) end),
    awful.key({ modkey,           }, "u", awful.client.urgent.jumpto),
    awful.key({ modkey,           }, "Tab",
        function ()
            awful.client.focus.history.previous()
            if client.focus then
                client.focus:raise()
            end
        end),
    awful.key({ altkey, "Shift"   }, "l",      function () awful.tag.incmwfact( 0.05)     end),
    awful.key({ altkey, "Shift"   }, "h",      function () awful.tag.incmwfact(-0.05)     end),
    awful.key({ modkey, "Shift"   }, "l",      function () awful.tag.incnmaster(-1)       end),
    awful.key({ modkey, "Shift"   }, "h",      function () awful.tag.incnmaster( 1)       end),
    awful.key({ modkey, "Control" }, "l",      function () awful.tag.incncol(-1)          end),
    awful.key({ modkey, "Control" }, "h",      function () awful.tag.incncol( 1)          end),
    awful.key({ modkey,           }, "space",  function () awful.layout.inc(layouts,  1)  end),
    awful.key({ modkey, "Shift"   }, "space",  function () awful.layout.inc(layouts, -1)  end),
    awful.key({ modkey, "Control" }, "n",      awful.client.restore),

    -- Standard program
    awful.key({ modkey,           }, "Return", function () awful.util.spawn(terminal) end),
    awful.key({ modkey, "Control" }, "r",      awesome.restart),
    awful.key({ modkey, "Shift"   }, "q",      awesome.quit),

	-- System
	awful.key({ modkey, "Shift"   }, "p", function () awful.util.spawn("poweroff") end),

    -- Widgets popups
    awful.key({ altkey,           }, "c",      function () lain.widgets.calendar:show(7) end),

    -- ALSA volume control
    awful.key({ }, "XF86AudioRaiseVolume",
        function ()
            os.execute(string.format("amixer set %s %s+", myvolumebar.channel, myvolumebar.step))
            myvolumebar.update()
        end),
    awful.key({ }, "XF86AudioLowerVolume",
        function ()
            os.execute(string.format("amixer set %s %s-", myvolumebar.channel, myvolumebar.step))
            myvolumebar.update()
        end),
    awful.key({ }, "XF86AudioMute",
        function ()
            os.execute(string.format("amixer set %s toggle", myvolumebar.channel))
            myvolumebar.update()
        end),

	-- On the fly useless gaps change
	awful.key({ modkey, "Control" }, "[", function () lain.util.useless_gaps_resize(8) end),
	awful.key({ modkey, "Control" }, "]", function () lain.util.useless_gaps_resize(-8) end),

    -- User programs
    awful.key({ modkey }, "q", function () awful.util.spawn("firefox") end),

    -- Prompt
    awful.key({ modkey }, "r", function () awful.util.spawn("rofi -show run -color-normal '#333333,#CCCCCC,#444444,#336699,#FFFFFF' -color-window '#222222,#111111' -bw 2 -font 'Input 14' -lines 8 -hide-scrollbar") end)
)

clientkeys = awful.util.table.join(
    awful.key({ modkey,           }, "f",      function (c) c.fullscreen = not c.fullscreen  end),
    awful.key({ modkey, "Shift"   }, "c",      function (c) c:kill()                         end),
    awful.key({ modkey, "Control" }, "space",  awful.client.floating.toggle                     ),
    awful.key({ modkey, "Control" }, "Return", function (c) c:swap(awful.client.getmaster()) end),
    awful.key({ modkey,           }, "o",      awful.client.movetoscreen                        ),
    awful.key({ modkey,           }, "t",      function (c) c.ontop = not c.ontop            end),
    awful.key({ modkey,           }, "n",
        function (c)
            -- The client currently has the input focus, so it cannot be
            -- minimized, since minimized clients can't have the focus.
            c.minimized = true
        end),
    awful.key({ modkey,           }, "m",
        function (c)
            c.maximized_horizontal = not c.maximized_horizontal
            c.maximized_vertical   = not c.maximized_vertical
        end)
)

-- Bind all key numbers to tags.
-- be careful: we use keycodes to make it work on any keyboard layout.
-- This should map on the top row of your keyboard, usually 1 to 9.
for i = 1, 9 do
    globalkeys = awful.util.table.join(globalkeys,
        -- View tag only.
        awful.key({ modkey }, "#" .. i + 9,
                  function ()
                        local screen = mouse.screen
                        local tag = awful.tag.gettags(screen)[i]
                        if tag then
                           awful.tag.viewonly(tag)
                        end
                  end),
        -- Toggle tag.
        awful.key({ modkey, "Control" }, "#" .. i + 9,
                  function ()
                      local screen = mouse.screen
                      local tag = awful.tag.gettags(screen)[i]
                      if tag then
                         awful.tag.viewtoggle(tag)
                      end
                  end),
        -- Move client to tag.
        awful.key({ modkey, "Shift" }, "#" .. i + 9,
                  function ()
                      if client.focus then
                          local tag = awful.tag.gettags(client.focus.screen)[i]
                          if tag then
                              awful.client.movetotag(tag)
                          end
                     end
                  end),
        -- Toggle tag.
        awful.key({ modkey, "Control", "Shift" }, "#" .. i + 9,
                  function ()
                      if client.focus then
                          local tag = awful.tag.gettags(client.focus.screen)[i]
                          if tag then
                              awful.client.toggletag(tag)
                          end
                      end
                  end))
end

clientbuttons = awful.util.table.join(
    awful.button({ }, 1, function (c) client.focus = c; c:raise() end),
    awful.button({ modkey }, 1, awful.mouse.client.move),
    awful.button({ modkey }, 3, awful.mouse.client.resize))

-- Set keys
root.keys(globalkeys)
-- }}}

-- {{{ Rules
awful.rules.rules = {
    -- All clients will match this rule.
    { rule = { },
      properties = { border_width = beautiful.border_width,
                     border_color = beautiful.border_normal,
                     focus = awful.client.focus.filter,
                     keys = clientkeys,
                     buttons = clientbuttons,
	                 size_hints_honor = false } },
	{ rule = { class = "Steam" },
      properties = { floating = true } },
	{ rule = { class = "Tixati" },
      properties = { floating = true } }
}
-- }}}

-- {{{ Signals
-- Signal function to execute when a new client appears.
local sloppyfocus_last = {c=nil}
client.connect_signal("manage", function (c, startup)
    -- Enable sloppy focus
    client.connect_signal("mouse::enter", function(c)
         if awful.layout.get(c.screen) ~= awful.layout.suit.magnifier
            and awful.client.focus.filter(c) then
             -- Skip focusing the client if the mouse wasn't moved.
             if c ~= sloppyfocus_last.c then
                 client.focus = c
                 sloppyfocus_last.c = c
             end
         end
     end)

    if not startup then
        -- Layout new floating windows if no initial position is already set
        if not c.size_hints.user_position and not c.size_hints.program_position then
            awful.placement.no_overlap(c)
            awful.placement.no_offscreen(c)
        end
    end

	-- Titlebar stuff
    if (c.type == "normal" or c.type == "dialog") and awful.layout.get(c.screen) ~= awful.layout.suit.max then
        -- buttons for the titlebar
        local buttons = awful.util.table.join(
                awful.button({ }, 1, function()
                    client.focus = c
                    c:raise()
                    awful.mouse.client.move(c)
                end)
                )

        -- Minimize, Maximize, Close buttons
        local right_layout = wibox.layout.fixed.horizontal()
        right_layout:add(awful.titlebar.widget.minimizebutton(c))
        right_layout:add(awful.titlebar.widget.maximizedbutton(c))
        right_layout:add(awful.titlebar.widget.closebutton(c))

        -- Dragable titlebar
        local middle_layout = wibox.layout.flex.horizontal()
        middle_layout:buttons(buttons)

        -- Now bring it all together
        local layout = wibox.layout.align.horizontal()
        layout:set_right(right_layout)
        layout:set_middle(middle_layout)

        awful.titlebar(c,{size=2, bg_normal=beautiful.border_normal, bg_focus=beautiful.fg_focus}):set_widget(layout)
		awful.titlebar.show(c)
    end
end)

-- Transparency
client.connect_signal("focus", function(c)
                              c.border_color = beautiful.border_focus
                              c.opacity = 1
                           end)
client.connect_signal("unfocus", function(c)
                                c.border_color = beautiful.border_normal
                                c.opacity = 0.90
                             end)
-- }}}

-- {{{ Arrange signal handler
for s = 1, screen.count() do screen[s]:connect_signal("arrange", function ()
        local clients = awful.client.visible(s)
        local layout  = awful.layout.getname(awful.layout.get(s))

        if #clients > 0 then -- Fine grained borders and floaters control
            for _, c in pairs(clients) do -- Floaters always have borders
				if (c.maximized_horizontal == true and c.maximized_vertical == true) or layout == "max" then
					c.border_width = 0
					awful.titlebar.hide(c)
				else
					c.border_width = beautiful.border_width
					awful.titlebar.show(c)
				end
            end
        end
      end)
end
-- }}}

